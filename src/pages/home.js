import React, {Component} from 'react';
import Bar from '../component/bar2.js'
import Man from '../assets/man.png'
import './css/home.css'

class Home extends Component{
    render() {
        return (
            <div>
            	<Bar/>
                <div className='page-home-man'>
                    <img src={Man} alt="man"/>
                </div>
            	<div className='page-home-text'>
                    <p>พี่เต้เองจะใครละ</p>
                    <p>F :Tanawat Chanhom</p>
                    <p>IG:|._.tae._.|</p>
                    <p>#itfreshy2019-Dev</p>
                    <p>#Backend</p>
                </div>
            </div>
        )
    }
}

export default Home;