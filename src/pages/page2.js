import React, {Component} from 'react';
import Bar from '../component/bar2'

class Page2 extends Component{
    render() {
        return (
            <div>
            	<Bar/>
                <div style={{marginTop: '50px'}}>
                    <h1>Page2</h1>
                </div>
            </div>
        )
    }
}

export default Page2;