import React, {Component} from 'react';
// import Bar from '../component/bar'
import './css/login.css'
import logo from '../assets/freshy-logo.2b990695.png'

class Login extends Component{
    constructor(props) {
        super(props);
        this.state = {
            user: 'Username',
            pass: 'Password'
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        // alert('A name was submitted: ' + this.state.user);
        this.props.history.push('/');
        event.preventDefault();
    }

    render() {
        return (
            <div className='login-container'>
                <div style={{height: '100%', width: '100%'}}>
                    <div className='login-dot1' style={{top: '250px', left: '-100px'}}/>
                    <div className='login-dot2' style={{top: '-50px', right: '-50px'}}/>
                </div>
                {/*<Bar/>*/}
                <div>
                    <div className='login-box'>
                        <img alt='logo' src={logo} className='login-img'/>
                        <form className='login-form' onSubmit={this.handleSubmit}>
                            {/*<label>*/}
                            {/*    <input className='login-input' type="text" name="user" value={this.state.user} onChange={this.handleChange} /><br/>*/}
                            {/*    <input className='login-input' type="text" name="pass" value={this.state.pass} onChange={this.handleChange} /><br/>*/}
                            {/*</label>*/}
                            <input className='login-button' type="submit" value="Login with Facebook"/>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;