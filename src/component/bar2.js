import React, {Component} from 'react';
import '../css/bar2.css';
// import logo from "../assets/freshy-logo.2b990695.png";

class Bar extends Component{
    render() {
        return (
            <div className='cpn-bar-container'>
                <div className='cpn-bar-name'>
                    {/*<img src={logo} alt='' className='cpn-bar-img'/>*/}
                    <p>TNW</p>
                </div>
                <input type="checkbox" name="chk" id="chk1"/>
                <label htmlFor="chk1" className="cpn-bar-label"/>

                <div className='cpn-bar-line-container'>
                    <div className='cpn-bar-line-box' >
                        <div className="cpn-bar-line"/>
                        <div className="cpn-bar-line"/>
                        <div className="cpn-bar-line"/>
                    </div>
                </div>
                <div className='cpn-bar-link-container'>
                    <div className='cpn-bar-label-box'/>
                    <div className='cpn-bar-link'>
                        <a href='/'>HOME</a>
                        <a href='/page1'>PAGE1</a>
                        <a href='/page2'>PAGE2</a>
                        <a href='/page3'>PAGE3</a>
                        <a href='/login'>LOGOUT</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default Bar;