import React, {Component} from 'react';
import Page1 from './pages/page1';
import Page2 from './pages/page2';
import Page3 from './pages/page3';
import Home from './pages/home';
import Login from './pages/login';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

class App extends Component{
    render() {
        return (
        	<Router>
        		<Switch>
        			<Route path='/' exact component={Home}/>
					<Route path='/login' component={Login}/>
		            <Route path='/page1' component={Page1}/>
		            <Route path='/page2' component={Page2}/>
		            <Route path='/page3' component={Page3}/>
	            </Switch>
            </Router>
        )
    }
}

export default App;
